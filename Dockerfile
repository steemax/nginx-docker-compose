FROM centos:7

RUN yum -y install epel-release
RUN yum -y install nginx
COPY app.conf /etc/nginx/conf.d/
COPY index.html /var/www/nginx/

ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]
