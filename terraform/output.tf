output "internal_ip_address_nginx-compose" {
  value = yandex_compute_instance.nginx-compose.network_interface.0.ip_address
}

output "external_ip_address_nginx-compose" {
  value = yandex_compute_instance.nginx-compose.network_interface.0.nat_ip_address
}

