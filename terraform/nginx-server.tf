resource "yandex_compute_instance" "nginx-compose" {
  name = "nginx-compose"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd80le4b8gt2u33lvubr" #Centos
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-nginx-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("users")}"
  }
}

resource "yandex_vpc_network" "network-nginx-1" {
  name = "network-nginx-1"
}

resource "yandex_vpc_subnet" "subnet-nginx-1" {
  name           = "subnet-nginx-1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-nginx-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}
