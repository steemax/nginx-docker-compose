Запускаем NGINX в docker, minikube, и через terraform (не имея ничего больше под рукой)
========================
nginx отдает одну единственную страничку HelloWorld (будет доступна на 80м порту + index.html), в nginx включен stub_status чтобы оттуда забирал данные nginx-prometheus-exporter, в prometheus настроен сбор метрик с экспортера, в grafana уже подключен data source нашего Prometheus и сделан dashboard с текущим статусом и количеством коннектов (просто для понимания процесса).

### Способы установки: ###
1. Локально используя Docker-compose
2. Локально в minikube
3. Terraform (если под рукой нет свободной тачки с docker, создадим и все накатим в yandex cloud)

### docker-compose ###
- клонируем репозиторий "git clone https://gitlab.com/steemax/nginx-docker-compose.git"
- "cd nginx-docker-compose.git"
- "docker-compose up -d" - соберет nginx с нужным нам конфигом (на самом деле, самым простым) из Dockerfile и поднимет все остальные контейнеры локально

*этот способ предполагает что у вас уже установлен и запущен docker и установлен docker-compose*

### minikube ###
- просто эпплаим манифесты из deployment_minikube.yaml "kuubectl apply -f ./deployment_minikube.yaml"

*этот способ предполагает что у вас уже установлен и запущен minikube, все ресурсы буддут созданы в nginx-docker namespace*

### Terraform ### 
- если вы из России (иначе первый пункт пропускаем), добавляем зеркало для скачивания провайдера, копируем файл из каталога terraform/.terraformrc этого репозитория - себе в хомяк "~/.terraformrc" с тем же именем
- заполняем переменные своими данными с YandexCloud в в файле cloud-yandex.auto.tfvars (креды и зона для создания instance)
- инициализируем провайдер terraform init
- проверяем что код - ок (не обязательно уже проверено) terraform validate
- применяем конфиги terraform apply, на выходе получите 2 адреса, Public это будет адрес к которому можно подключиться (если нужно) по ssh (admin/admin) и на нем же будут доступны все сервисы (grafana, prometheus, nginx, exporter)
- ждем около 15-ти минут пока отработает cloud-init который запустит установку и запуск всех контейнеров на созданом в YC сервере

*этот способ предполагает что у вас уже есть аккаунт в Yandec Cloud, но не требует знаний как им пользоваться)))) *

### креды ###
при первом способе (docker-compose) приложения доступны по стандартным портам
   - http://your_ip_address/index.html - старничка нашего NGINX с которого собираются метрики
   - http://your_ip_address:9090 - страничка Prometheus
   - http://your_ip_address:9113 - nginx-prometheus-exporter
   - http://your_ip_address:3000 - grafana (admin/admin), дашборт в каталоге General

при втором способе (minikube) приложения доступны по:
   - http://your_ip_address:31180/index.html - старничка нашего NGINX с которого собираются метрики
   - http://your_ip_address:32090 - страничка Prometheus
   - http://your_ip_address:32113 - nginx-prometheus-exporter
   - http://your_ip_address:32000 - grafana (admin/admin), дашборт в каталоге General

при третьем способе (terraform) приложения доступны по стандартным портам (спустя примерно 15 минут как завершился apply):
   - http://your_cloud_ip_address/index.html - старничка нашего NGINX с которого собираются метрики
   - http://your_cloud_ip_address:9090 - страничка Prometheus
   - http://your_cloud_ip_address:9113 - nginx-prometheus-exporter
   - http://your_cloud_ip_address:3000 - grafana (admin/admin), дашборт в каталоге General
*your_cloud_ip_address будет в выводе terraform по завершении apply или здесь: "terraform output external_ip_address_nginx-compose", username/password для доступа по ssh - admin/admin, не забываем после теста делать destroy*
